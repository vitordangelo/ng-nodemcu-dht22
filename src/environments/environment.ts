// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAum9LCN2y2BjHp7Ec8cmenyS7tPBOutnw',
    authDomain: 'fir-nodejs-dcdeb.firebaseapp.com',
    databaseURL: 'https://fir-nodejs-dcdeb.firebaseio.com',
    projectId: 'fir-nodejs-dcdeb',
    storageBucket: 'fir-nodejs-dcdeb.appspot.com',
    messagingSenderId: '1025557252743'
  }
};
