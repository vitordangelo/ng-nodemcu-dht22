import { Component } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  lastValue: Observable<any>;
  readsAtTime: any;
  temperatures = [];
  last1440Values = [];
  events: string[] = [];

  constructor(db: AngularFireDatabase) {
    this.lastValue = db.list('dht22', ref => ref.limitToLast(1)).valueChanges();

    this.readsAtTime = db.list('dht22', ref => ref.orderByChild('timestap').startAt(1510273513401).endAt(1510273806773))
      .valueChanges()
      .subscribe(items => {
        this.temperatures.push(items);

        const temperatures = [];
        for (const data of this.temperatures[0]) {
          // console.log(data.temperature);
          temperatures.push(data.temperature);
        }
      });

    db.list('dht22', ref => ref.limitToLast(1440))
      .valueChanges()
      .subscribe(values => {
        this.last1440Values.push(values);
        const last1440Temperatures = [];
        const last1440Time = [];
        for (const value of this.last1440Values[0]) {
          last1440Temperatures.push(value.temperature);
          last1440Time.push(value.timestap);
        }
      });
  }

  onSubmit(f) {
    console.log(f._datepickerInput.value);
    const date = new Date(f._datepickerInput.value);
    console.log(date);
  }
}
